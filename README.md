# Access Token Validator

## Description

This is a very small utility to validate access tokens that are emitted from: https://auth.kudze.lt

## How to use

1. Configure enviroment variable `JWT_PUBLIC_KEY` to be public key from: https://auth.kudze.lt (We may introduce rotating keys in future)
2. Use `AccessTokenValidator` service to decode access token.

## Unit tests

In order to run unit tests you need to:

1. Install composer dependencies: `composer install`
2. Run phpunit tests: `./vendor/bin/phpunit`