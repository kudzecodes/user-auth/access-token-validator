<?php

namespace Kudze\AccessTokenValidator\Model;

class User
{
    public function __construct(
        public readonly string $uuid,
        public readonly string $email,
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly string $createdAt,
        public readonly string $updatedAt,
    )
    {

    }
}