<?php

namespace Kudze\AccessTokenValidator;

class Details
{
    const string ALGO = 'RS512';

    const string TYPE_ACCESS_TOKEN = "ACCESS";
    const string TYPE_REFRESH_TOKEN = "REFRESH";
}