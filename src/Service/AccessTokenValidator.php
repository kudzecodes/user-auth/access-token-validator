<?php

namespace Kudze\AccessTokenValidator\Service;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Kudze\AccessTokenValidator\Details;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use InvalidArgumentException;
use DomainException;
use Kudze\AccessTokenValidator\Model\User;
use UnexpectedValueException;
use stdClass;

class AccessTokenValidator
{
    protected string $jwtPublicKey;

    public function __construct()
    {
        $this->jwtPublicKey = $_ENV['JWT_PUBLIC_KEY'];
    }

    /**
     * @throws InvalidTokenTypeException    Provided token is refresh token
     * @throws InvalidArgumentException     Provided key/key-array was empty or malformed
     * @throws DomainException              Provided JWT is malformed
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException         Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     */
    public function decodeAccessToken(string $token): stdClass
    {
        $decoded = JWT::decode($token, new Key($this->jwtPublicKey, Details::ALGO));

        if ($decoded->type !== Details::TYPE_ACCESS_TOKEN)
            throw new InvalidTokenTypeException();

        return $decoded;
    }

    /**
     * @throws InvalidTokenTypeException    Provided token is refresh token
     * @throws InvalidArgumentException     Provided key/key-array was empty or malformed
     * @throws DomainException              Provided JWT is malformed
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException         Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     */
    public function decodeUserFromAccessToken(string $token): User
    {
        return $this->getUserFromDecodedAccessToken($this->decodeAccessToken($token));
    }

    public function getUserFromDecodedAccessToken(stdClass $accessToken): User
    {
        return new User(
            $accessToken->user->uuid,
            $accessToken->user->email,
            $accessToken->user->first_name,
            $accessToken->user->last_name,
            $accessToken->user->created_at,
            $accessToken->user->updated_at,
        );
    }
}