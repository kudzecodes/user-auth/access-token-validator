<?php

namespace Tests\Service;

use Carbon\Carbon;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use UnexpectedValueException;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use Kudze\AccessTokenValidator\Service\AccessTokenValidator;
use Tests\TestCase;
use Tests\Traits\WorksWithResources;

class AccessTokenValidatorTest extends TestCase
{
    use WorksWithResources;

    protected static function getToken(string $path): string
    {
        return self::getContentsInResources('tokens/' . $path);
    }

    protected static function getValidAccessToken(): string
    {
        return self::getToken('access_valid.jwt');
    }

    protected static function getInvalidAccessToken(): string
    {
        return self::getToken('access_invalid.jwt');
    }

    protected static function getValidRefreshToken(): string
    {
        return self::getToken('refresh_valid.jwt');
    }

    protected static function getInstance(): AccessTokenValidator
    {
        return new AccessTokenValidator();
    }

    protected function tearDown(): void
    {
        JWT::$timestamp = null;
    }

    /**
     * With expired token should raise ExpiredException
     *
     * @throws InvalidTokenTypeException
     */
    public function testExpiredToken_decodeAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(ExpiredException::class);

        JWT::$timestamp = Carbon::parse('2024-02-12 23:14:00')->getTimestamp();
        $instance->decodeAccessToken(self::getValidAccessToken());
    }

    /**
     * With expired token should raise ExpiredException
     *
     * @throws InvalidTokenTypeException
     */
    public function testExpiredToken_decodeUserFromAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(ExpiredException::class);

        JWT::$timestamp = Carbon::parse('2024-02-12 23:14:00')->getTimestamp();
        $instance->decodeUserFromAccessToken(self::getValidAccessToken());
    }

    /**
     * With future token should raise BeforeValidException
     *
     * @throws InvalidTokenTypeException
     */
    public function testFutureToken_decodeAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(BeforeValidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-10 23:14:00')->getTimestamp();
        $instance->decodeAccessToken(self::getValidAccessToken());
    }

    /**
     * With future token should raise BeforeValidException
     *
     * @throws InvalidTokenTypeException
     */
    public function testFutureToken_decodeUserFromAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(BeforeValidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-10 23:14:00')->getTimestamp();
        $instance->decodeUserFromAccessToken(self::getValidAccessToken());
    }

    /**
     * With invalid token signature should raise SignatureInvalidException
     *
     * @return void
     * @throws InvalidTokenTypeException
     */
    public function testInvalidAccessToken_decodeAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(SignatureInvalidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->decodeAccessToken(self::getInvalidAccessToken());
    }

    /**
     * With invalid token signature should raise SignatureInvalidException
     *
     * @return void
     * @throws InvalidTokenTypeException
     */
    public function testInvalidAccessToken_decodeUserFromAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(SignatureInvalidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->decodeUserFromAccessToken(self::getInvalidAccessToken());
    }

    /**
     * With valid refresh token we should raise InvalidTokenTypeException
     *
     * @return void
     * @throws InvalidTokenTypeException
     */
    public function testValidRefreshToken_decodeAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(InvalidTokenTypeException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->decodeAccessToken(self::getValidRefreshToken());
    }

    /**
     * With valid refresh token we should raise InvalidTokenTypeException
     *
     * @return void
     * @throws InvalidTokenTypeException
     */
    public function testValidRefreshToken_decodeUserFromAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(InvalidTokenTypeException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->decodeUserFromAccessToken(self::getValidRefreshToken());
    }

    /**
     * With invalid jwt token we should get UnexpectedValueException.
     *
     * @throws InvalidTokenTypeException
     */
    public function testInvalidToken_decodeAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(UnexpectedValueException::class);
        $instance->decodeAccessToken('i am not valid token');
    }

    /**
     * With invalid jwt token we should get UnexpectedValueException.
     *
     * @throws InvalidTokenTypeException
     */
    public function testInvalidToken_decodeUserFromAccessToken()
    {
        $instance = self::getInstance();

        $this->expectException(UnexpectedValueException::class);
        $instance->decodeUserFromAccessToken('i am not valid token');
    }

    /**
     * With valid access token we should operate.
     *
     * @return void
     * @throws InvalidTokenTypeException
     */
    public function testValidAccessToken()
    {
        $instance = self::getInstance();
        $expectedUserUuid = "9b4fad57-0181-419a-8427-7901ee3a1ce9";
        $accessToken = self::getValidAccessToken();

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $decoded = $instance->decodeAccessToken($accessToken);
        $this->assertSame($expectedUserUuid, $decoded->user->uuid);

        $decodedUser = $instance->getUserFromDecodedAccessToken($decoded);
        $this->assertSame($expectedUserUuid, $decodedUser->uuid);

        $decodedUserDirectly = $instance->decodeUserFromAccessToken($accessToken);
        $this->assertEquals($decodedUser, $decodedUserDirectly);
    }
}