<?php

namespace Tests;


use PHPUnit\Framework\TestCase as BaseTestCase;
use Tests\Traits\WorksWithResources;

abstract class TestCase extends BaseTestCase
{
    use WorksWithResources;

    public static function setUpBeforeClass(): void
    {
        $_ENV['JWT_PUBLIC_KEY'] = self::getContentsInResources('jwt.pub');
    }
}